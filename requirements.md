# SRS001 ClickHouse `CREATE USER` Statement Software Requirements Specification

## Requirements

### RQ.SRS001.User.Create
version: 1.0

ClickHouse SHALL support creating user accounts using the `CREATE USER` statement.

### RQ.SRS001.User.Create.IfNotExists
version: 1.0

ClickHouse SHALL support the `IF NOT EXISTS` clause in the `CREATE USER` statement to skip raising an exception
if a user with the same name already exists and SHALL raise an exception if the `IF NOT EXISTS` clause is not specified
but a user already exists.

### RQ.SRS001.User.Create.Replace
version: 1.0

ClickHouse SHALL support `OR REPLACE` clause in the `CREATE USER` statement to replace existing
user account if already exists.
