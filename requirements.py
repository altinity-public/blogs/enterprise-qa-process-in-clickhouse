# These requirements were auto generated
# from software requirements specification (SRS)
# document by TestFlows v1.6.201009.1190249.
# Do not edit by hand but re-generate instead
# using 'tfs requirements generate' command.
from testflows.core import Requirement

RQ_SRS001_User_Create = Requirement(
        name='RQ.SRS001.User.Create',
        version='1.0',
        priority=None,
        group=None,
        type=None,
        uid=None,
        description=(
        'ClickHouse SHALL support creating user accounts using the `CREATE USER` statement.\n'
        '\n'
        ),
        link=None)

RQ_SRS001_User_Create_IfNotExists = Requirement(
        name='RQ.SRS001.User.Create.IfNotExists',
        version='1.0',
        priority=None,
        group=None,
        type=None,
        uid=None,
        description=(
        'ClickHouse SHALL support the `IF NOT EXISTS` clause in the `CREATE USER` statement to skip raising an exception\n'
        'if a user with the same name already exists and SHALL raise an exception if the `IF NOT EXISTS` clause is not specified\n'
        'but a user already exists.\n'
        '\n'
        ),
        link=None)

RQ_SRS001_User_Create_Replace = Requirement(
        name='RQ.SRS001.User.Create.Replace',
        version='1.0',
        priority=None,
        group=None,
        type=None,
        uid=None,
        description=(
        'ClickHouse SHALL support `OR REPLACE` clause in the `CREATE USER` statement to replace existing\n'
        'user account if already exists.\n'
        ),
        link=None)
