#!/usr/bin/env python3
from clickhouse_driver import Client
from clickhouse_driver.errors import ServerException

from testflows.core import *
from testflows.asserts import error
from requirements import *

@TestStep(When)
def execute_query(self, client, query):
    """Execute query and capture exception if any.
    """
    r = None
    query_exc = None

    with By("executing query", description=f"{query}", format_description=False):
        try:
            r = client.execute(query)
        except ServerException as exc:
            query_exc = exc

    return r, query_exc

@TestStep(Given)
def open_client(self, host="localhost"):
    """Open client connection.
    """
    try:
        client = Client(host)
        yield client
    finally:
        with By("closing client connection"):
            client.disconnect()

@TestScenario
@Requirements(RQ_SRS001_User_Create_IfNotExists("1.0"))
def verify_if_not_exists_clause(self, host="localhost"):
    """Verify that the `CREATE USER` statement does not raise an exception
    if the `IF NOT EXISTS` clause is specified and a user with the same name
    already exists and raises exception if `IF NOT EXISTS` clause is not specified.
    """
    client = None
    user = "test_user"
    error_code = 493
    error_message = f"DB::Exception: user `{user}`: cannot insert because user `{user}` already exists in local directory"

    try:
        with Given("I have a client connection", description=f"to {host}"):
            client = open_client(host=host)

        with When("I create a user"):
            r, exc = execute_query(client=client, query=f"CREATE USER {user}")

            with Then("there should be no error"):
                assert exc is None, error()

        with When("I check user definition"):
            r, exc = execute_query(client=client, query=f"SHOW CREATE USER {user}")

            expected = f"CREATE USER {user}"
            with Then("it should match the expected", description=f"{expected}"):
                assert expected == r[0][0], error()

        with When("I try to create a user with the same name without the `IF NOT EXISTS` clause"):
            r, exc = execute_query(client=client, query=f"CREATE USER {user}")

            with Then("I expect an error", description=f"{error_code} {error_message}"):
                assert exc is not None and error_code == exc.code and error_message in exc.message, error()

        with When("I try to create a user with the same name with the `IF NOT EXISTS` clause"):
            r, exc = execute_query(client=client, query=f"CREATE USER IF NOT EXISTS {user}")

            with Then("there should be no error"):
                assert exc is None, error()

    finally:
        if client:
            with Finally("I delete the user", flags=TE):
                execute_query(client=client, query=f"DROP USER IF EXISTS {user}")

@TestScenario
@Requirements(RQ_SRS001_User_Create_Replace("1.0"))
def verify_replace_clause(self, host="localhost"):
    """Verify that the `CREATE USER` statement with the `OR REPLACE` clause
    replaces the definition of the user if a user with the same name
    already exists.
    """
    client = None
    user = "test_user"
    error_code = 493
    error_message = f"DB::Exception: user `{user}`: cannot insert because user `{user}` already exists in local directory"

    try:
        with Given("I have a client connection", description=f"to {host}"):
            client = open_client(host=host)

        with When("I create a user"):
            r, exc = execute_query(client=client, query=f"CREATE USER {user}")

            with Then("there should be no error"):
                assert exc is None, error()

        with When("I check user definition"):
            r, exc = execute_query(client=client, query=f"SHOW CREATE USER {user}")

            expected = f"CREATE USER {user}"
            with Then("it should match the expected", description=f"{expected}"):
                assert expected == r[0][0], error()

        with When("I try to create a user with the same name without the `OR REPLACE` clause"):
            r, exc = execute_query(client=client, query=f"CREATE USER {user}")

            with Then("I expect an error", description=f"{error_code} {error_message}"):
                assert exc is not None and error_code == exc.code and error_message in exc.message, error()

        with When("I try to create a user with the same name with the `OR REPLACE` clause with plaintext password specified"):
            r, exc = execute_query(client=client, query=f"CREATE USER OR REPLACE {user} IDENTIFIED WITH PLAINTEXT_PASSWORD BY 'hello there'")

            with Then("there should be no error"):
                assert exc is None, error()

        with When("I again check user definition"):
            r, exc = execute_query(client=client, query=f"SHOW CREATE USER {user}")

            expected = f"CREATE USER {user} IDENTIFIED WITH plaintext_password"
            with Then("it should contain the new definition", description=f"{expected}"):
                assert expected == r[0][0], error()

    finally:
        if client:
            with Finally("I delete the user", flags=TE):
                execute_query(client=client, query=f"DROP USER IF EXISTS {user}")


@TestFeature
@Name("create user")
@Requirements(RQ_SRS001_User_Create("1.0"))
def feature(self):
    for scenario in loads(current_module(), Scenario):
        scenario()

if main():
    feature()
