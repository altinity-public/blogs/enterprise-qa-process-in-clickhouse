# Enterprise QA Process in ClickHouse

The [Altinity] Way to Building Enterprise QA Process in ClickHouse - Test Project.

## How To Run

### Step 1: install [ClickHouse]

See https://clickhouse.tech/docs/en/getting-started/install/.

### Step 2: install [TestFlows] open-source test framework

```bash
pip3 install testflows
```

### Step 3: install [Python ClickHouse driver]

```bash
pip3 install clickhouse_driver
```

### Step 4: run the tests

Run the test using

```bash
python3 regression.py -o short
```

or simply

```bash
./regression.py -o short
```

You should see the output similar to this.

```bash
$ ./regression.py -o short
Feature create user
  Requirements
    RQ.SRS001.User.Create
      version 1.0
  Scenario verify if not exists clause
    Verify that the `CREATE USER` statement does not raise an exception
    if the `IF NOT EXISTS` clause is specified and a user with the same name
    already exists and raises exception if `IF NOT EXISTS` clause is not specified.
    Requirements
      RQ.SRS001.User.Create.IfNotExists
        version 1.0
    Arguments
      host
        localhost
    Given I have a client connection
      to localhost
    OK
    When I create a user
      By executing query
        CREATE USER test_user
      OK
      Then there should be no error
      OK
    OK
    When I check user definition
      By executing query
        SHOW CREATE USER test_user
      OK
      Then it should match the expected
        CREATE USER test_user
      OK
    OK
    When I try to create a user with the same name without the `IF NOT EXISTS` clause
      By executing query
        CREATE USER test_user
      OK
      Then I expect an error
        493 DB::Exception: user `test_user`: cannot insert because user `test_user` already exists in local directory
      OK
    OK
    When I try to create a user with the same name with the `IF NOT EXISTS` clause
      By executing query
        CREATE USER IF NOT EXISTS test_user
      OK
      Then there should be no error
      OK
    OK
    Finally I delete the user
      By executing query
        DROP USER IF EXISTS test_user
      OK
    OK
    Finally I clean up
      By closing client connection
      OK
    OK
  OK
  Scenario verify replace clause
    Verify that the `CREATE USER` statement with the `OR REPLACE` clause
    replaces the definition of the user if a user with the same name
    already exists.
    Requirements
      RQ.SRS001.User.Create.Replace
        version 1.0
    Arguments
      host
        localhost
    Given I have a client connection
      to localhost
    OK
    When I create a user
      By executing query
        CREATE USER test_user
      OK
      Then there should be no error
      OK
    OK
    When I check user definition
      By executing query
        SHOW CREATE USER test_user
      OK
      Then it should match the expected
        CREATE USER test_user
      OK
    OK
    When I try to create a user with the same name without the `OR REPLACE` clause
      By executing query
        CREATE USER test_user
      OK
      Then I expect an error
        493 DB::Exception: user `test_user`: cannot insert because user `test_user` already exists in local directory
      OK
    OK
    When I try to create a user with the same name with the `OR REPLACE` clause with plaintext password specified
      By executing query
        CREATE USER OR REPLACE test_user IDENTIFIED WITH PLAINTEXT_PASSWORD BY 'hello there'
      OK
      Then there should be no error
      OK
    OK
    When I again check user definition
      By executing query
        SHOW CREATE USER test_user
      OK
      Then it should contain the new definition
        CREATE USER test_user IDENTIFIED WITH plaintext_password
      OK
    OK
    Finally I delete the user
      By executing query
        DROP USER IF EXISTS test_user
      OK
    OK
    Finally I clean up
      By closing client connection
      OK
    OK
  OK
OK

Passing

✔ [ OK ] /create user/verify if not exists clause
✔ [ OK ] /create user/verify replace clause
✔ [ OK ] /create user

1 suite (1 ok)
2 scenarios (2 ok)
37 steps (37 ok)

Total time 84ms

Executed on Oct 19,2020 10:23
TestFlows.com Open-Source Software Testing Framework v1.6.201018.1231552
```

[Altinity]: https://altinity.com
[ClickHouse]: https://clickhouse.tech/
[TestFlows]: https://testflows.com
[Python ClickHouse driver]: https://github.com/mymarilyn/clickhouse-driver